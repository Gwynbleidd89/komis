﻿using Komis.Models;
using Komis.ViewModels.Abstract;
using Komis.ViewModels.Commands;
using System.Runtime.CompilerServices;

namespace Komis.ViewModels
{
    class ClickCountersViewModel : ViewModelBase
    {
        int _areaCounter1;
        int _areaCounter2;
        int _areaCounter3;

        public ClickCountersViewModel()
        {
            FirstBtnClick = new ActionCommand(
                () => AreaCounter1 = Counter.Increment(AreaCounter1));

            SettingsBtnClick = new ActionCommand(
                () => AreaCounter2 = Counter.Increment(AreaCounter2));

            ThirdBtnClick = new ActionCommand(
                () => AreaCounter3 = Counter.Increment(AreaCounter3));
        }

        /// <summary>
        /// Счетчик первой области.
        /// </summary>
        public int AreaCounter1
        {
            get => _areaCounter1;
            set => SetField(ref _areaCounter1, value);
        }

        /// <summary>
        /// Счетчик второй области.
        /// </summary>
        public int AreaCounter2
        {
            get => _areaCounter2;
            set => SetField(ref _areaCounter2, value);
        }

        /// <summary>
        /// Счетчик третьей области.
        /// </summary>
        public int AreaCounter3
        {
            get => _areaCounter3;
            set => SetField(ref _areaCounter3, value);
        }

        public ActionCommand FirstBtnClick { get; }

        public ActionCommand SettingsBtnClick { get; }

        public ActionCommand ThirdBtnClick { get; }

        /// <summary>
        /// Указывает было ли установлено значение поля, при истине уведомляет
        /// зарегистрированные объекты события PropertyChanged базового класса.
        /// </summary>
        /// <param name="propertyName">Имя свойства, по умолчанию соответствует
        /// имени свойства/метода вызывающего кода.</param>
        protected virtual bool SetField(ref int field, int value, [CallerMemberName] string propertyName = "")
        { 
            if (field == value)
            {
                return false;
            }

            field = value > 0 ? value : 0;
            OnPropertyChanged(propertyName);
            return true;
        }
    }
}
