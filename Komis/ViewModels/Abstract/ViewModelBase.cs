﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Komis.ViewModels.Abstract
{
    abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Уведомляет зарегистрированные объекты события <see cref="PropertyChanged"/>.
        /// </summary>
        /// <param name="propertyName">Имя свойства, по умолчанию сооветствует
        /// имени свойства/метода вызывающего кода.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            Volatile.Read(ref PropertyChanged)
                ?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
