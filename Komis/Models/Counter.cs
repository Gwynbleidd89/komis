﻿namespace Komis.Models
{
    static class Counter
    {
        /// <summary>
        /// Приращает значение на единицу, при переполнении вернется ноль.
        /// </summary>
        /// <param name="currentValue"></param>
        public static int Increment(int currentValue)
        {
            long longValue = currentValue;

            if(++longValue > int.MaxValue)
            {
                longValue = 0;
            }

            return (int)longValue;
        }
    }
}
